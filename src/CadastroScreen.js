import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, ImageBackground, TouchableOpacity, Alert } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import sheets from './axios';

const CadastroScreen = () => {
    const navigation = useNavigation();
    const [nome, setNome] = useState('');
    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');

    const handleCadastro = async () => {
        try {
            // Verifica se todos os campos estão preenchidos
            if (nome === '' || email === '' || senha === '') {
        
                return;
            }
    
            // Envio dos dados para a API usando axios
            const response = await sheets.postUser({ nome, email, senha });
    
            // Verifica se a resposta foi bem-sucedida
            if (response.status === 200 || response.status === 201) {
                // Cadastro bem-sucedido: navega para a tela 'Users'
                Alert.alert('Cadastro realizado com sucesso!');
                navigation.navigate('Users');
                
                // Limpa os campos após o cadastro
                setNome('');
                setEmail('');
                setSenha('');
            } else {
                // Se a resposta não foi bem-sucedida, mostra uma mensagem de erro
                const errorMessage = response.data.message || 'Falha ao realizar cadastro. Por favor, tente novamente.';

            }
        } catch (error) {
            // Verifica se o erro é um erro de Axios
            if (error.response && error.response.status === 400) {
                // Extrai a mensagem de erro específica da resposta
                const errorMessage = error.response.data.message || 'Solicitação inválida. Por favor, verifique os dados e tente novamente.';
                Alert.alert('Erro', errorMessage);
            } else {
                // Lida com outros erros
                console.error('Erro ao cadastrar aluno:', error);
                Alert.alert('Erro ao cadastrar aluno:', error.message || 'Ocorreu um erro inesperado.');
            }
        }
    };
    
    

    return (
        <ImageBackground style={styles.backgroundImage}>
            <View style={styles.container}>
                <View style={styles.box}>
                    <Text style={styles.title}>Cadastro</Text>
                    <TextInput
                        style={styles.input}
                        placeholder="Nome"
                        value={nome}
                        onChangeText={setNome}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Email"
                        value={email}
                        onChangeText={setEmail}
                        keyboardType="email-address"
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Senha"
                        value={senha}
                        onChangeText={setSenha}
                        secureTextEntry
                    />
                    
                    {/* Botão de cadastro */}
                    <Button title="Cadastrar" onPress={handleCadastro} />

                    {/* Botão de voltar */}
                    <TouchableOpacity style={styles.buttonVoltar} onPress={() => navigation.goBack()}>
                        <Text style={styles.buttonText}>Voltar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ImageBackground>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    box: {
        backgroundColor: '#00447c',
        borderRadius: 10,
        padding: 20,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        color: 'white',
    },
    input: {
        height: 40,
        width: 200,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 20,
        paddingHorizontal: 10,
        backgroundColor: '#fff',
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
    },
    buttonVoltar: {
        backgroundColor: '#00447c',
        paddingVertical: 14,
        paddingHorizontal: 30,
        borderRadius: 5,
        marginBottom: 20,
    },
    buttonText: {
        color: '#ffffff',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});

export default CadastroScreen;
