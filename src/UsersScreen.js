import React, { useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import axios from 'axios';
import Icon from 'react-native-vector-icons/MaterialIcons'; // Biblioteca para ícones

const UsersScreen = () => {
  const [selectedRoomId, setSelectedRoomId] = useState(null);
  const [reservations, setReservations] = useState([]);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    axios.get('http://10.89.234.201:8081/api/users')
      .then(response => {
        if (Array.isArray(response.data)) {
          setUsers(response.data);
        } else {
          console.error('Formato inválido dos dados retornados:', response.data);
        }
      })
      .catch(error => {
        console.error('Erro ao obter usuários:', error);
      });
  }, []);

  const availableRooms = [
    { id: 1, name: 'Sala de Português', capacity: 32, teacher: 'Prof. Silva' },
    { id: 2, name: 'Sala de Matemática', capacity: 30, teacher: 'Prof. Santos' },
    { id: 3, name: 'Sala de História', capacity: 25, teacher: 'Prof. Costa' },
    { id: 4, name: 'Sala de Biologia', capacity: 28, teacher: 'Prof. Oliveira' },
    { id: 5, name: 'Sala de Física', capacity: 27, teacher: 'Prof. Rodrigues' },
    { id: 6, name: 'Sala de Química', capacity: 26, teacher: 'Prof. Ferreira' },
  ];

  const reserveRoom = () => {
    if (selectedRoomId) {
      const room = availableRooms.find((room) => room.id === selectedRoomId);
      if (room) {
        setReservations((prevReservations) => [...prevReservations, room]);
        setSelectedRoomId(null);
      }
    }
  };

  // Função para deletar reserva
  const deleteReservation = (index) => {
    setReservations((prevReservations) =>
      prevReservations.filter((_, i) => i !== index)
    );
  };

  const renderRoomOptions = () => {
    return availableRooms.map((room) => (
      <View key={room.id} style={styles.roomOptionContainer}>
        <Button
          title={room.name}
          onPress={() => setSelectedRoomId(room.id)}
          disabled={selectedRoomId === room.id}
        />
        <View style={styles.separator}></View>
      </View>
    ));
  };

  const renderRoomDescription = () => {
    const room = availableRooms.find((room) => room.id === selectedRoomId);
    if (room) {
      return (
        <View style={styles.roomDescription}>
          <Text style={styles.roomDescriptionText}>Nome: {room.name}</Text>
          <Text style={styles.roomDescriptionText}>Capacidade: {room.capacity}</Text>
          <Text style={styles.roomDescriptionText}>Professor: {room.teacher}</Text>
          <Button title="Reservar" onPress={reserveRoom} />
        </View>
      );
    }
    return null;
  };

  // Adiciona um botão de deletar à frente de cada reserva
  const renderReservations = () => {
    return (
      <View style={styles.reservationsBox}>
        <Text style={styles.reservationsTitle}>Reservas:</Text>
        {reservations.map((reservation, index) => (
          <View key={index} style={styles.reservationItem}>
            {/* Exibe o nome da reserva */}
            <Text style={styles.reservationText}>{reservation.name}</Text>
            {/* Botão de deletar reserva */}
            <TouchableOpacity style={styles.deleteButton} onPress={() => deleteReservation(index)}>
                <Icon name="delete" size={20} color="red" />
            </TouchableOpacity>
          </View>
        ))}
      </View>
    );
  };

  return (
    <ImageBackground source={require('../assets/escolar.jpg')} style={styles.backgroundImage}>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.content}>
          <View style={styles.optionsAndDescriptionContainer}>
            <View style={styles.roomOptionsContainer}>
              <Text style={styles.title}>Opções de Salas:</Text>
              {renderRoomOptions()}
            </View>
            <View style={styles.descriptionContainer}>
              <Text style={styles.descriptionTitle}>Descrição da Sala:</Text>
              {renderRoomDescription()}
            </View>
          </View>
          {renderReservations()}
          <View style={styles.usersContainer}>
            {users.map((user, index) => (
              <View key={index}>
                <Text>{user.name}</Text>
              </View>
            ))}
          </View>
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  content: {
    alignItems: 'center',
  },
  optionsAndDescriptionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  roomOptionContainer: {
    marginHorizontal: 5,
  },
  separator: {
    height: 10,
  },
  roomOptionsContainer: {
    flex: 1,
  },
  descriptionContainer: {
    flex: 1,
    marginLeft: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    padding: 10,
  },
  descriptionTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  roomDescription: {
    alignItems: 'center',
  },
  roomDescriptionText: {
    textAlign: 'center',
  },
  reservationsBox: {
    marginTop: 20,
    width: '50%',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
  },
  reservationsTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  reservationItem: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  reservationText: {
    flex: 1,
  },
  deleteButton: {
    padding: 5,
  },
  usersContainer: {
    marginTop: 20,
  },
  usersTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
});

export default UsersScreen;
