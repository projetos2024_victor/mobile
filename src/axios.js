import axios from "axios";

const api = axios.create({
    baseURL: "http://10.89.234.201:5000/api",
    headers: {
        'accept': 'application/json'
    }
});

const sheets = {
    postUser: (user) => api.post("/cadastroAluno", user),
    logUser: (user) => api.post("/loginAluno", user)
};

export default sheets;
